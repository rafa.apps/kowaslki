from IPython.display import clear_output

def plot_history(history_dict, title=''):
  x = range(len(history_dict['acc']))
  _, (ax1, ax2) = plt.subplots(1, 2, figsize=(18, 5), sharey=False)
  ax1.plot(x, history_dict['acc'], label='acc', color='r')
  ax1.plot(x, history_dict['val_acc'], label='val_acc', color='b')
  ax1.set_xlabel('epochs')
  ax1.set_title('%s Accuracy' % title)
  ax1.legend()
  
  
  ax2.plot(x, history_dict['loss'], label='loss', color='r')
  ax2.plot(x, history_dict['val_loss'], label='val_loss', color='b')
  ax2.set_xlabel('epochs')
  ax2.set_title('%s Loss' % title)
  ax2.legend()
  
  plt.show()
  
class PlotHistory(Callback):
    def on_train_begin(self, logs=None):
        self.epoch = []
        self.logs = defaultdict(list)
        self.history = {}
        
    def on_epoch_end(self, epoch, logs=None):
        #print(logs)
        #print(logs.keys())
        for key, metric in logs.items():
          self.logs[key].append(metric)

        clear_output(wait=True)
        f, (ax1, ax2) = plt.subplots(2, 1, sharex=True, figsize=(10, 6))
                
        #logs = logs or {}
        self.epoch.append(epoch+1)
        #for k, v in logs.items():
        #    self.history.setdefault(k, []).append(v)
  
        x = list(self.epoch)
        '''
        print(x)
        print(logs['accuracy'])
        print(logs['val_accuracy'])
        print(logs['loss'])
        print(logs['val_loss'])
        '''
        ax1.plot(x, self.logs['acc'], label='acc', color='r', marker='>')
        ax1.plot(x, self.logs['val_acc'], label='val_acc', color='b', marker='>')
        ax1.set_xlabel('epochs')
        ax1.set_title('Accuracy')
        ax1.legend()
  
        ax2.plot(x, self.logs['loss'], label='loss', color='r', marker='>')
        ax2.plot(x, self.logs['val_loss'], label='val_loss', color='b', marker='>')
        ax2.set_xlabel('epochs')
        ax2.set_title('Loss')
        ax2.legend()
        plt.show()
        print('val_acc: %0.3f' % logs['val_acc'])
        
#http://scikit-learn.org/stable/auto_examples/model_selection/plot_confusion_matrix.html
import itertools

def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          width=8,
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    #print(cm)

    f, ax = plt.subplots(1, 1, figsize=(width, width))
    ax.imshow(cm, interpolation='nearest', cmap=cmap)
    ax.set_title(title)
    #plt.colorbar()
    tick_marks = np.arange(len(classes))
    ax.set_xticks(tick_marks)
    ax.set_yticks(tick_marks)
    ax.set_xticklabels(classes, rotation=90)
    ax.set_yticklabels(classes)
    ax.grid(False)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        ax.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    ax.set_ylabel('True label')
    ax.set_xlabel('Predicted label')
    #axis[ax].axis('off')
    plt.tight_layout()
    
    return cm
